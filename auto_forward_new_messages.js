// This Script once setup, auto forwards all messages from a chat to another.

var tg_bot_token = "" // Replace with Telegram Bot Token
var admin = "5854304441"
var from_channel = [-1001845572485] // add as many as you want in this
var to_channel = [-1001549539244] // Don't add too many. upto 1-10 should work fine. Recommended is 5 Max.
var blocked_keywords = [] // eg. ["this", "that"]
var whitelist_mode = false
var whitelist_keywords = [] // eg. ["this", "that"]
var minimum_size = 0 // For 5 MB set 5242880
var is_text = true
var is_photo = true
var is_video = true
var is_document = true
var change_caption = false
var modify_caption = false
var caption = "\nTest Caption\n\n@HashHackers" // \n if for Next Line
var is_audio = true
var is_sticker = true
var is_voice = true
var is_animation = true
var is_others = true
var debug = true

async function handleRequest(request) {
    var url = new URL(request.url);
    var path = url.pathname;
    var hostname = url.hostname;
    if (path == '/') {
        var setwebhook = await fetch("https://api.telegram.org/bot" + tg_bot_token + "/setWebhook?url=https://" + hostname + "/bot&drop_pending_updates=true&max_connections=100", {
            method: "GET",
        });
        if (setwebhook.ok) {
            return new Response("Web Hook Set", {
                headers: {
                    'content-type': 'text/html;charset=UTF-8',
                },
            });
        } else {
            return new Response("Unable to Set Web Hook, something went wrong.", {
                headers: {
                    'content-type': 'text/html;charset=UTF-8',
                },
            });
        }
    } else if (path == '/bot') {
        var data = JSON.stringify(await request.json());
        var obj = JSON.parse(data);
        if (debug) {
            await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Incoming request from " + from_channel + " : " + data, {
                method: "GET",
            });
        }
        if (obj.hasOwnProperty('channel_post')) {
            if (from_channel.includes(obj.channel_post.sender_chat.id) && obj.channel_post.hasOwnProperty('text') && is_text) {
                for (let i = 0; i < to_channel.length; i++) {
                    console.log("runtime: " + i)
                    var res = await fetch("https://api.telegram.org/bot" + tg_bot_token + "/copyMessage?disable_web_page_preview=true&chat_id=" + to_channel[i] + "&from_chat_id=" + from_channel + "&message_id=" + obj.channel_post.message_id, {
                        method: "GET",
                    });
                    var jsondata = await res.text();
                    var copydata1 = JSON.parse(jsondata);
                    if (res.ok) {
                        console.log("Incoming Message Copied.")
                        if (debug) {
                            await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=RES OK: " + from_channel + " : " + jsondata, {
                                method: "GET",
                            });
                        }
                    } else if (res.status == 429) {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Flood Wait.\n\nRetry after " + copydata1.parameters.retry_after + " Seconds.", {
                            method: "GET",
                        });
                        return new Response(jsondata, {
                            status: 429,
                            headers: {
                                'content-type': 'application/json',
                            },
                        });
                    } else {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Problem at Channel (not 429) " + to_channel[i] + " : " + jsondata, {
                            method: "GET",
                        });
                    }
                }
                return new Response("OK!", {
                    status: 200,
                    headers: {
                        'content-type': 'application/json',
                    },
                });
            } else if (from_channel.includes(obj.channel_post.sender_chat.id) && obj.channel_post.hasOwnProperty('photo') && is_photo) {
                for (let i = 0; i < to_channel.length; i++) {
                    console.log("runtime: " + i)
                    var res = await fetch("https://api.telegram.org/bot" + tg_bot_token + "/copyMessage?disable_web_page_preview=true&chat_id=" + to_channel[i] + "&from_chat_id=" + from_channel + "&message_id=" + obj.channel_post.message_id, {
                        method: "GET",
                    });
                    var jsondata = await res.text();
                    var copydata1 = JSON.parse(jsondata);
                    if (res.ok) {
                        console.log("Incoming Message Copied.")
                        if (debug) {
                            await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=RES OK: " + from_channel + " : " + jsondata, {
                                method: "GET",
                            });
                        }
                    } else if (res.status == 429) {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Flood Wait.\n\nRetry after " + copydata1.parameters.retry_after + " Seconds.", {
                            method: "GET",
                        });
                        return new Response(jsondata, {
                            status: 429,
                            headers: {
                                'content-type': 'application/json',
                            },
                        });
                    } else {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Problem at Channel (not 429) " + to_channel[i] + " : " + jsondata, {
                            method: "GET",
                        });
                    }
                }
                return new Response("OK!", {
                    status: 200,
                    headers: {
                        'content-type': 'application/json',
                    },
                });
            } else if (from_channel.includes(obj.channel_post.sender_chat.id) && obj.channel_post.hasOwnProperty('video') && is_video) {
                if (obj.channel_post.video.hasOwnProperty('file_name')) {
                    var file_name = obj.channel_post.video.file_name
                    for (var i = 0; i < blocked_keywords.length; i++) {
                        if (file_name.toLowerCase().includes(blocked_keywords[i].toLowerCase())) {
                            console.log("Blocked keyword found: " + blocked_keywords[i]);
                            if (debug) {
                                await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Incoming request from " + from_channel + " : " + "Blocked keyword found: " + blocked_keywords[i], {
                                    method: "GET",
                                });
                            }
                            return new Response("OK", {
                                status: 200,
                                headers: {
                                    "content-type": "application/json",
                                },
                            })
                            break; // Stop searching for other blocked keywords
                        }
                    }
                    if (whitelist_mode) {
                        var keywordFound = false;
                        for (var i = 0; i < whitelist_keywords.length; i++) {
                            if (file_name.toLowerCase().includes(whitelist_keywords[i].toLowerCase())) {
                                console.log("Whitelist keyword found: " + whitelist_keywords[i]);
                                keywordFound = true;
                                break;
                            }
                        }
                        if (!keywordFound) {
                            if (debug) {
                                await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Incoming request from " + from_channel + " : " + "No Whitelisted keyword found", {
                                    method: "GET",
                                });
                            }
                            return new Response("OK", {
                                status: 200,
                                headers: {
                                    "content-type": "application/json",
                                },
                            })
                        }
                    }
                }

                for (let i = 0; i < to_channel.length; i++) {
                    var file_size = obj.channel_post.video.file_size;
                    if (file_size < minimum_size) {
                        console.log("Min File Size not Satisfied, Ignoring.")
                        return new Response("Ignoring Due to File Size", {
                            status: 200,
                            headers: {
                                'content-type': 'application/json',
                            },
                        });
                    }
                    if (obj.channel_post.hasOwnProperty('caption') && modify_caption) {
                        var new_caption = "&caption=" + obj.channel_post.caption + caption
                    } else if (change_caption) {
                        var new_caption = "&caption=" + caption
                    } else {
                        var new_caption = ""
                    }
                    console.log("runtime: " + i)
                    var res = await fetch("https://api.telegram.org/bot" + tg_bot_token + "/copyMessage?disable_web_page_preview=true&chat_id=" + to_channel[i] + "&from_chat_id=" + from_channel + "&message_id=" + obj.channel_post.message_id + new_caption, {
                        method: "GET",
                    });
                    var jsondata = await res.text();
                    var copydata1 = JSON.parse(jsondata);
                    if (res.ok) {
                        console.log("Incoming Message Copied.")
                        if (debug) {
                            await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=RES OK: " + from_channel + " : " + jsondata, {
                                method: "GET",
                            });
                        }
                    } else if (res.status == 429) {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Flood Wait.\n\nRetry after " + copydata1.parameters.retry_after + " Seconds.", {
                            method: "GET",
                        });
                        return new Response(jsondata, {
                            status: 429,
                            headers: {
                                'content-type': 'application/json',
                            },
                        });
                    } else {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Problem at Channel (not 429) " + to_channel[i] + " : " + jsondata, {
                            method: "GET",
                        });
                    }
                }
                return new Response("OK!", {
                    status: 200,
                    headers: {
                        'content-type': 'application/json',
                    },
                });
            } else if (from_channel.includes(obj.channel_post.sender_chat.id) && obj.channel_post.hasOwnProperty('document') && is_document) {
                if (obj.channel_post.document.hasOwnProperty('file_name')) {
                    var file_name = obj.channel_post.document.file_name
                    for (var i = 0; i < blocked_keywords.length; i++) {
                        if (file_name.toLowerCase().includes(blocked_keywords[i].toLowerCase())) {
                            console.log("Blocked keyword found: " + blocked_keywords[i]);
                            return new Response("OK", {
                                status: 200,
                                headers: {
                                    "content-type": "application/json",
                                },
                            })
                            break; // Stop searching for other blocked keywords
                        }
                    }
                    if (whitelist_mode) {
                        var keywordFound = false;
                        for (var i = 0; i < whitelist_keywords.length; i++) {
                            if (file_name.toLowerCase().includes(whitelist_keywords[i].toLowerCase())) {
                                console.log("Whitelist keyword found: " + whitelist_keywords[i]);
                                keywordFound = true;
                                break;
                            }
                        }
                        if (!keywordFound) {
                            return new Response("OK", {
                                status: 200,
                                headers: {
                                    "content-type": "application/json",
                                },
                            })
                        }
                    }
                }
                for (let i = 0; i < to_channel.length; i++) {
                    var file_size = obj.channel_post.document.file_size;
                    if (file_size < minimum_size) {
                        console.log("Min File Size not Satisfied, Ignoring.")
                        return new Response("Ignoring Due to File Size", {
                            status: 200,
                            headers: {
                                'content-type': 'application/json',
                            },
                        });
                    }
                    if (obj.channel_post.hasOwnProperty('caption') && modify_caption) {
                        var new_caption = "&caption=" + obj.channel_post.caption + caption
                    } else if (change_caption) {
                        var new_caption = "&caption=" + caption
                    } else {
                        var new_caption = ""
                    }
                    console.log("runtime: " + i)
                    var res = await fetch("https://api.telegram.org/bot" + tg_bot_token + "/copyMessage?disable_web_page_preview=true&chat_id=" + to_channel[i] + "&from_chat_id=" + from_channel + "&message_id=" + obj.channel_post.message_id + new_caption, {
                        method: "GET",
                    });
                    var jsondata = await res.text();
                    var copydata1 = JSON.parse(jsondata);
                    if (res.ok) {
                        console.log("Incoming Message Copied.")
                        if (debug) {
                            await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=RES OK: " + from_channel + " : " + jsondata, {
                                method: "GET",
                            });
                        }
                    } else if (res.status == 429) {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Flood Wait.\n\nRetry after " + copydata1.parameters.retry_after + " Seconds.", {
                            method: "GET",
                        });
                        return new Response(jsondata, {
                            status: 429,
                            headers: {
                                'content-type': 'application/json',
                            },
                        });
                    } else {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Problem at Channel (not 429) " + to_channel[i] + " : " + jsondata, {
                            method: "GET",
                        });
                    }
                }
                return new Response("OK!", {
                    status: 200,
                    headers: {
                        'content-type': 'application/json',
                    },
                });
            } else if (from_channel.includes(obj.channel_post.sender_chat.id) && obj.channel_post.hasOwnProperty('audio') && is_audio) {
                for (let i = 0; i < to_channel.length; i++) {
                    var file_size = obj.channel_post.audio.file_size;
                    if (file_size < minimum_size) {
                        console.log("Min File Size not Satisfied, Ignoring.")
                        return new Response("Ignoring Due to File Size", {
                            status: 200,
                            headers: {
                                'content-type': 'application/json',
                            },
                        });
                    }
                    if (change_caption) {
                        var new_caption = "&caption=" + caption
                    } else {
                        var new_caption = ""
                    }
                    console.log("runtime: " + i)
                    var res = await fetch("https://api.telegram.org/bot" + tg_bot_token + "/copyMessage?disable_web_page_preview=true&chat_id=" + to_channel[i] + "&from_chat_id=" + from_channel + "&message_id=" + obj.channel_post.message_id + new_caption, {
                        method: "GET",
                    });
                    var jsondata = await res.text();
                    var copydata1 = JSON.parse(jsondata);
                    if (res.ok) {
                        console.log("Incoming Message Copied.")
                        if (debug) {
                            await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=RES OK: " + from_channel + " : " + jsondata, {
                                method: "GET",
                            });
                        }
                    } else if (res.status == 429) {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Flood Wait.\n\nRetry after " + copydata1.parameters.retry_after + " Seconds.", {
                            method: "GET",
                        });
                        return new Response(jsondata, {
                            status: 429,
                            headers: {
                                'content-type': 'application/json',
                            },
                        });
                    } else {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Problem at Channel (not 429) " + to_channel[i] + " : " + jsondata, {
                            method: "GET",
                        });
                    }
                }
                return new Response("OK!", {
                    status: 200,
                    headers: {
                        'content-type': 'application/json',
                    },
                });
            } else if (from_channel.includes(obj.channel_post.sender_chat.id) && obj.channel_post.hasOwnProperty('sticker') && is_sticker) {
                for (let i = 0; i < to_channel.length; i++) {
                    console.log("runtime: " + i)
                    var res = await fetch("https://api.telegram.org/bot" + tg_bot_token + "/copyMessage?disable_web_page_preview=true&chat_id=" + to_channel[i] + "&from_chat_id=" + from_channel + "&message_id=" + obj.channel_post.message_id, {
                        method: "GET",
                    });
                    var jsondata = await res.text();
                    var copydata1 = JSON.parse(jsondata);
                    if (res.ok) {
                        console.log("Incoming Message Copied.")
                        if (debug) {
                            await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=RES OK: " + from_channel + " : " + jsondata, {
                                method: "GET",
                            });
                        }
                    } else if (res.status == 429) {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Flood Wait.\n\nRetry after " + copydata1.parameters.retry_after + " Seconds.", {
                            method: "GET",
                        });
                        return new Response(jsondata, {
                            status: 429,
                            headers: {
                                'content-type': 'application/json',
                            },
                        });
                    } else {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Problem at Channel (not 429) " + to_channel[i] + " : " + jsondata, {
                            method: "GET",
                        });
                    }
                }
                return new Response("OK!", {
                    status: 200,
                    headers: {
                        'content-type': 'application/json',
                    },
                });
            } else if (from_channel.includes(obj.channel_post.sender_chat.id) && obj.channel_post.hasOwnProperty('voice') && is_video) {
                for (let i = 0; i < to_channel.length; i++) {
                    console.log("runtime: " + i)
                    var res = await fetch("https://api.telegram.org/bot" + tg_bot_token + "/copyMessage?disable_web_page_preview=true&chat_id=" + to_channel[i] + "&from_chat_id=" + from_channel + "&message_id=" + obj.channel_post.message_id, {
                        method: "GET",
                    });
                    var jsondata = await res.text();
                    var copydata1 = JSON.parse(jsondata);
                    if (res.ok) {
                        console.log("Incoming Message Copied.")
                        if (debug) {
                            await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=RES OK: " + from_channel + " : " + jsondata, {
                                method: "GET",
                            });
                        }
                    } else if (res.status == 429) {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Flood Wait.\n\nRetry after " + copydata1.parameters.retry_after + " Seconds.", {
                            method: "GET",
                        });
                        return new Response(jsondata, {
                            status: 429,
                            headers: {
                                'content-type': 'application/json',
                            },
                        });
                    } else {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Problem at Channel (not 429) " + to_channel[i] + " : " + jsondata, {
                            method: "GET",
                        });
                    }
                }
                return new Response("OK!", {
                    status: 200,
                    headers: {
                        'content-type': 'application/json',
                    },
                });
            } else if (from_channel.includes(obj.channel_post.sender_chat.id) && obj.channel_post.hasOwnProperty('animation') && is_animation) {
                for (let i = 0; i < to_channel.length; i++) {
                    console.log("runtime: " + i)
                    var res = await fetch("https://api.telegram.org/bot" + tg_bot_token + "/copyMessage?disable_web_page_preview=true&chat_id=" + to_channel[i] + "&from_chat_id=" + from_channel + "&message_id=" + obj.channel_post.message_id, {
                        method: "GET",
                    });
                    var jsondata = await res.text();
                    var copydata1 = JSON.parse(jsondata);
                    if (res.ok) {
                        console.log("Incoming Message Copied.")
                        if (debug) {
                            await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=RES OK: " + from_channel + " : " + jsondata, {
                                method: "GET",
                            });
                        }
                    } else if (res.status == 429) {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Flood Wait.\n\nRetry after " + copydata1.parameters.retry_after + " Seconds.", {
                            method: "GET",
                        });
                        return new Response(jsondata, {
                            status: 429,
                            headers: {
                                'content-type': 'application/json',
                            },
                        });
                    } else {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Problem at Channel (not 429) " + to_channel[i] + " : " + jsondata, {
                            method: "GET",
                        });
                    }
                }
                return new Response("OK!", {
                    status: 200,
                    headers: {
                        'content-type': 'application/json',
                    },
                });
            } else if (from_channel.includes(obj.channel_post.sender_chat.id) && is_others) {
                for (let i = 0; i < to_channel.length; i++) {
                    console.log("runtime: " + i)
                    var res = await fetch("https://api.telegram.org/bot" + tg_bot_token + "/copyMessage?disable_web_page_preview=true&chat_id=" + to_channel[i] + "&from_chat_id=" + from_channel + "&message_id=" + obj.channel_post.message_id, {
                        method: "GET",
                    });
                    var jsondata = await res.text();
                    var copydata1 = JSON.parse(jsondata);
                    if (res.ok) {
                        console.log("Incoming Message Copied.")
                        if (debug) {
                            await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=RES OK: " + from_channel + " : " + jsondata, {
                                method: "GET",
                            });
                        }
                    } else if (res.status == 429) {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Flood Wait.\n\nRetry after " + copydata1.parameters.retry_after + " Seconds.", {
                            method: "GET",
                        });
                        return new Response(jsondata, {
                            status: 429,
                            headers: {
                                'content-type': 'application/json',
                            },
                        });
                    } else {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Problem at Channel (not 429) " + to_channel[i] + " : " + jsondata, {
                            method: "GET",
                        });
                    }
                }
                return new Response("OK!", {
                    status: 200,
                    headers: {
                        'content-type': 'application/json',
                    },
                });
            }
        } else if (obj.hasOwnProperty('message')) {
            if (obj.message.hasOwnProperty('text')) {
                var usertext = obj.message.text
                if (usertext.startsWith('/start')) {
                    await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + obj.message.from.id + "&reply_to_message_id=" + obj.message.message_id + "&text=I'm already UP Boss!", {
                        method: "GET",
                    });
                } else {
                    await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + obj.message.from.id + "&reply_to_message_id=" + obj.message.message_id + "&text=Bhadoo Cloner Bot Private Server.", {
                        method: "GET",
                    });
                }
            }
        }
        return new Response("OK", {
            status: 200,
            headers: {
                'content-type': 'application/json',
            },
        });
    } else {
        return new Response("OK", {
            status: 200,
            headers: {
                'content-type': 'application/json',
            },
        });
    }
}

addEventListener('fetch', event => {
    return event.respondWith(handleRequest(event.request));
});
